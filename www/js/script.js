window.onload = () => {

    const numberBtns = document.querySelectorAll('[data-number]');
    const operationBtns = document.querySelectorAll('[data-operation]');
    const equalsBtn = document.querySelector('[data-equals]');
    const colorBtn = document.querySelector('[data-color-picker]');
    const deleteBtn = document.querySelector('[data-delete]');
    const undoBtn = document.querySelector('[data-undo]');
    const currentOperandTextElement = document.querySelector('[data-current-operand]');
    const previousOperandTextElement = document.querySelector('[data-previous-operand]');

    console.log({numberBtns});
    console.log({operationBtns});
    console.log({equalsBtn});
    console.log({colorBtn});
    console.log({deleteBtn});
    console.log({undoBtn});
    console.log({currentOperandTextElement});
    console.log({previousOperandTextElement});
 
    const c = new Calculator(previousOperandTextElement, currentOperandTextElement);
    
    numberBtns.forEach( button => {
        button.addEventListener('click', e => {
            c.appendNumber(button.innerText);
            c.updateDisplay();
        });
    });
    
    operationBtns.forEach( button => {
        button.addEventListener('click', e => {
            c.chooseOperation(button.innerText);
            c.updateDisplay();
        });
    });
    
    equalsBtn.addEventListener('click', button => {
        c.compute();
        c.updateDisplay();
    });
    
    deleteBtn.addEventListener('click', button => {
        c.delete();
        c.updateDisplay();
    });
    
    undoBtn.addEventListener('click', button => {
        c.clear();
        c.updateDisplay();
    });

    let colorPicker = new Axentix.Modal('#color-picker');
    console.log({colorPicker});

    const colorBtns = document.querySelectorAll('[data-color]');
    console.log(colorBtns);

    colorBtns.forEach( button => {
        button.addEventListener('click', e => {
            let card = document.querySelector('.card');
            let colors = new Array('neu-white', 'neu-grey', 'neu-blueGrey', 'neu-blue', 'neu-babyBlue', 'neu-lightRed', 'neu-pink', 'neu-orange');
            colors.forEach(color => {
                if(card.classList.contains(color) && color != button.id) {
                    card.classList.remove(color);
                } else {
                    card.classList.add(button.id);
                }
            });
            console.log(button.id);
        })
    } )
}